module Main where

import FIR.Examples.JuliaSet.Application
  ( juliaSet )

main :: IO ()
main = juliaSet
